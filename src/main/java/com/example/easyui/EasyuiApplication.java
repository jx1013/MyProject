package com.example.easyui;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@MapperScan(value = "com.example.easyui.dao")
@PropertySource("messages.properties")
public class EasyuiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyuiApplication.class, args);
    }

}
