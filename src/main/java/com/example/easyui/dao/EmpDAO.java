package com.example.easyui.dao;

import com.example.easyui.pojo.Emp;
import com.example.easyui.vo.EmpVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmpDAO {
    int deleteByPrimaryKey(Integer empno);

    int insert(Emp record);

    int insertSelective(Emp record);

    Emp selectByPrimaryKey(Integer empno);

    int updateByPrimaryKeySelective(Emp record);

    int updateByPrimaryKey(Emp record);

    List<Emp> allEmps(@Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize, @Param("vo") EmpVO vo);
}