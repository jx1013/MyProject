package com.example.easyui.dao;

import com.example.easyui.pojo.Dept;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DeptDAO {
    int deleteByPrimaryKey(Integer deptno);

    int insert(Dept record);

    int insertSelective(Dept record);

    Dept selectByPrimaryKey(Integer deptno);

    int updateByPrimaryKeySelective(Dept record);

    int updateByPrimaryKey(Dept record);
    @Select("select * from dept")
    List<Dept> allDepts();
}