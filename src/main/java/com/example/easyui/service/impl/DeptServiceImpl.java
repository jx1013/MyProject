package com.example.easyui.service.impl;

import com.example.easyui.dao.DeptDAO;
import com.example.easyui.pojo.Dept;
import com.example.easyui.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptDAO deptDAO;
    @Override
    public List<Dept> allDepts() {

        return deptDAO.allDepts();
    }
}
