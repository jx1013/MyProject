package com.example.easyui.service.impl;

import com.example.easyui.dao.EmpDAO;
import com.example.easyui.pojo.Emp;
import com.example.easyui.service.EmpService;
import com.example.easyui.vo.EmpVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpDAO empDAO;

    @Override
    public List<Emp> findAll(Integer currentPage, Integer pageSize, EmpVO vo) {
        return empDAO.allEmps(currentPage,pageSize,vo);
    }

    @Override
    public Emp getDetail(Integer empno) {
        return empDAO.selectByPrimaryKey(empno);
    }

    @Override
    public void updateEmp(Emp emp) {
        empDAO.updateByPrimaryKeySelective(emp);
    }

    @Override
    public Emp empInfo(Integer empno) {
        return empDAO.selectByPrimaryKey(empno);
    }
}
