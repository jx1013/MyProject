package com.example.easyui.service;

import com.example.easyui.pojo.Emp;
import com.example.easyui.vo.EmpVO;

import java.util.List;

public interface EmpService {
    List<Emp> findAll(Integer currentPage, Integer pageSize, EmpVO vo);

    Emp getDetail(Integer empno);

    void updateEmp(Emp emp);

    Emp empInfo(Integer empno);
}
