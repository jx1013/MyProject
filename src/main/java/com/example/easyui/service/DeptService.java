package com.example.easyui.service;

import com.example.easyui.pojo.Dept;
import com.example.easyui.pojo.Dept;

import java.util.List;

public interface DeptService {

    List<Dept> allDepts();
}
