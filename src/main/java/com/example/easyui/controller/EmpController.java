package com.example.easyui.controller;

import com.example.easyui.pojo.Dept;
import com.example.easyui.pojo.Emp;
import com.example.easyui.service.DeptService;
import com.example.easyui.service.EmpService;
import com.example.easyui.vo.EmpVO;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class EmpController {
    @Autowired
    private EmpService empService;
    @Autowired
    private DeptService deptService;
    @RequestMapping("/loadEmps")
    @ResponseBody
    public Map<String,Object> loadEmps(@RequestParam(value="page",required = false,defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "limit",required = false,defaultValue = "10") Integer rows, EmpVO vo){

        Map<String,Object> result = new HashMap();
        List<Emp> list = empService.findAll(pageNum,rows,vo);
        PageInfo pageInfo = new PageInfo(list);
        result.put("data",pageInfo.getList());
        result.put("code",0);
        result.put("count",pageInfo.getTotal());
        System.out.println(empService.getDetail(7499));
        return result;
    }


    @RequestMapping("/loadDepts")
    @ResponseBody
    public List<Dept> loadDepts(Emp emp)
    {
        return deptService.allDepts();
    }
}
